# syntax=docker/dockerfile:1

#Build stage
FROM golang:1.17-buster AS build

WORKDIR /app
COPY go.mod go.mod 

RUN apt-get update && apt-get install libzmq3-dev libczmq-dev -y
RUN go mod download
COPY . .
RUN go build -o worker

#Deployable image
FROM ubuntu:20.04

WORKDIR /app
RUN apt-get update && apt-get install libzmq3-dev libczmq-dev -y

COPY --from=build /app/worker ./worker

EXPOSE 8080

# USER nonroot:nonroot

ENTRYPOINT ["./worker"]