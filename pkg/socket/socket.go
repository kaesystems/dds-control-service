package socket

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.com/kaesystems/dds-control-service/pkg/models"
	"gopkg.in/zeromq/goczmq.v4"
)

//To handle socket connection with ZMQ

type Socket struct {
	pullSocket *goczmq.Sock
	data       models.DataInfo
}

//By default initalize Pull socket and bind them to project
func (s *Socket) Init() {
	var err error
	s.pullSocket, err = goczmq.NewRouter("tcp://127.0.0.1:5555")
	if err != nil {
		panic(err)
	}

}

func (s *Socket) ReadFromSocket() error {
	reply, err := s.pullSocket.RecvMessage()
	if err != nil {
		s.pullSocket.Destroy()
		time.Sleep(time.Second * 2)
		s.Init()
		return err
	}

	log.Printf("Data updated!")
	err = json.Unmarshal(reply[1], &s.data)
	if err != nil {
		return err
	}
	return nil
}

func (s *Socket) Close() {
	s.pullSocket.Destroy()
}
func (s *Socket) GetData() (*models.DataInfo, error) {
	return &s.data, nil
}
