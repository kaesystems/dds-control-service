package models

type DataInfo struct {
	Timestamp int     `json:"timestamp"`
	Topics    []Topic `json:"topics"`
}
type Topic struct {
	TopicName   string       `json:"topic_name"`
	DataWriters []DataWriter `json:"data_writers"`
	DataReaders []DataReader `json:"data_readers"`
	Latency     string       `json:"latency,omitempty"`
	Matched     string       `json:"matched,omitempty"`
}
type DataWriter struct {
	DataWriterName string            `json:"data_writer_name"`
	Locator        []string          `json:"locator"`
	Metrics        DataWriterMetrics `json:"metrics"`
}
type DataReader struct {
	DataReaderName string            `json:"data_reader_name"`
	Locator        []string          `json:"locator"`
	Metrics        DataReaderMetrics `json:"metrics"`
}

type DataWriterMetrics struct {
	DataCount             string `json:"data_count"`
	GapCount              string `json:"gap_count"`
	HeartbeatCount        string `json:"heartbeat_count"`
	PublicationThroughput string `json:"publication_throughput"`
	ResentData            string `json:"resent_data"`
	SampleDatas           string `json:"sample_datas"`
}

type DataReaderMetrics struct {
	AcknackCount           string `json:"acknack_count"`
	NackfragCount          string `json:"nackfrag_count"`
	SubscriptionThroughput string `json:"subscription_throughput"`
}
