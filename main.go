package main

import (
	"encoding/json"
	"log"
	"net/http"

	"bitbucket.com/kaesystems/dds-control-service/pkg/socket"
)

var s socket.Socket

func greet(w http.ResponseWriter, r *http.Request) {
	data, err := s.GetData()
	if err != nil {
		w.Write([]byte(err.Error()))
	}
	json.NewEncoder(w).Encode(&data)
}

func main() {
	s.Init()
	go func() {
		for {
			if err := s.ReadFromSocket(); err != nil {
				log.Println(err)
			}
		}

	}()
	log.Printf("Socket initialized.")
	http.HandleFunc("/", greet)
	http.ListenAndServe(":5500", nil)
}
