## DDS Control Service

DDS control Service expose DDS statistics backend data as a REST endpoint to use collected data.
![DDS Environment](docs/overview.png)
